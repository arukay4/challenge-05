const express = require("express");
const fs = require ('fs');
const PORT = process.env.PORT || 8000;
const path = require('path');

const app = express();

app.set('view engine', 'ejs');
app.use(express.json())
app.use(express.urlencoded())


// Path untuk file statis
const PUBLIC_PATH = path.join(__dirname, '/views');
app.use(express.static(PUBLIC_PATH))
app.use(express.static(path.join(__dirname, './views')))

//Routing Api dan Render 
const webRouter = require('./routes/web.router')
const apiRouter = require('./routes/api.router')

app.use('/', webRouter)
app.use('/api', apiRouter)


app.listen(PORT, () => {
    console.log (`Express sudah online di http://localhost:${PORT}`);
});