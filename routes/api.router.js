const express = require('express')
const router = express.Router()
const {getAllSize} = require('../controllers/api/size.controller')
const {getAllCars, newCars, showCar, editCars, deleteCar} = require('../controllers/api/cars.controller')


router.get('/size', getAllSize)
router.get('/cars', getAllCars)
router.post('/cars', newCars)
router.get('/cars/:id', showCar)
router.post('/cars/:id', editCars) 
// router.delete('/cars/:id', deleteCar)
router.get('/cars/delete/:id', deleteCar)


module.exports = router
