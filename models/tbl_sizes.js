'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_sizes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  tbl_sizes.init({
    name_size: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'tbl_sizes',
  });
  return tbl_sizes;
};