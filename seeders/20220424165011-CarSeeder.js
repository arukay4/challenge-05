'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

     await queryInterface.bulkInsert('tbl_cars', [
      {
        nama:'DMC DeLorean',
        harga:500000,
        url_image:"https://i.ibb.co/37sQgjw/DeLorean.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        nama:'BMW M3 E30',
        harga:250000,
        url_image:"https://i.ibb.co/28CsXH0/E30.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        nama:'Honda Civic EK9 Type-R',
        harga:300000,
        url_image:"https://i.ibb.co/mzMTRvj/EK9.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        nama:'Nissa Stagea R34 Wagon GT-R',
        harga:100000,
        url_image:"https://i.ibb.co/5c819sZ/Stagea-Wagon.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:3
      },
      {
        nama:'Toyota MR2 Supercharged',
        harga:700000,
        url_image:"https://i.ibb.co/5Wy34PH/1988-toyota-mr2-supercharged-153227619965ef66e7dff9f9876-DSC-9973.webp",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        nama:'Toyota Sprinter Trueno GT-APEX',
        harga:1500000,
        url_image:"https://i.ibb.co/Zmqpvhb/AE86.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        nama:'Nissan Skyline R30',
        harga:400000,
        url_image:"https://i.ibb.co/W6YdwJj/R30.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:2
      },
      {
        nama:'Nissan Skyline R32',
        harga:500000,
        url_image:"https://i.ibb.co/Yf25WSc/R32.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:2
      }
    ], {});

  },


  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
